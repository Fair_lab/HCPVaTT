# Pipeline Validator and Triage Tool `pipevatt`

This is a [`Small Project`](https://aircwiki.ohsu.edu/ProjectGuidlines)

Version number `0.2.1`

Custodians:
- Anders Perrone
- (formerly) Philip Robinson

Release targets:

- exahead1
- rushmore


This program should help you to triage and identify error subjects in
a described file heirarchy. To resolve some common points of confusion

- `whitelist` : list of subject IDs to include
- `blacklist` : list of subjects to skip (applied after whitelist)

If no `whitelist` file is provided the tool should use all subjects reachable by
your configuration file.

The status values should be as listed.

- not_started = 4
- incomplete = 2
- failed = 3
- succeded = 1
- unchecked = 999

## Configs

Every time you run a `pipevatt` command, you will need to provide a configuration file.
These can be `yaml` or `json` with the correct file extension.
The configuration file describes the pipeline that you are reviewing.
You can retrieve existing config file by running the `pipevatt config` command.

To list the existing config templates, use the command `pipevatt config`.

```bash
$ pipevatt config
rushmore_adhd.json
exahead1_theresa.json
exahead1_parkinsons.json
RDS_abcd.json
exahead1_abcd.json
exahead1_abcd.yml
exahead1_abide.json
```

In order to add new config files to the config templates, add them to the following path 

`~/.local/lib/python2.7/site-packages/pipevatt/configs/`

For example:

`/home/users/uriartel/.local/lib/python2.7/site-packages/pipevatt/configs/`

To retrieve one of these configuration file templates, use the same command with the `--name` parameter.

```bash
$ pipevatt config --name exahead1_abcd.yml
```

The goal of this config is to describe a processing pipeline, by subject path. The pattern is
`<processed>/<study>/<subject_pattern>/<visit_pattern>/<pipe_version>/<oneclick_version>`

As an example:

`/home/exacloud/lustre1/fnl_lab/data/HCP/processed/ABIDE/ABIDEI/ABIDEI-CMU/0031632/visit/HCP_release_20170910_v1.3/hcponeclick/`

```bash
$ cat configs/exahead1_abide.json
{
    pipeline_descriptor:/home/exacloud/lustre1/fnl_lab/code/internal/pipelines/hcponeclick_all/pipelines.json,
    pipeline_label:hcp_standard_SB,
    processed:/home/exacloud/lustre1/fnl_lab/data/HCP/processed/ABIDE/ABIDEI/,
    subject_pattern:00*,
    visit_pattern:visit,
    pipe_version:HCP_release_20170910_v1.3,
    oneclick_version:hcponeclick,
    studies: [
        ABIDEI-CMU,
        ABIDEI-LEUVEN,
    ],
    ignored:[]
}
```

And as a second example:

`/home/exacloud/lustre1/fnl_lab/data/HCP/processed/ABCD_GE_SEFMNoT2/sub-NDARINV008N6H7B/ses-baselineYear1Arm1/HCP_release_20170910_v1.4/hcponeclick_all`

```bash
$ cat exahead1_abcd.yml
pipeline_descriptor: /home/exacloud/lustre1/fnl_lab/code/internal/pipelines/hcponeclick/module_options.yml
pipeline_label: abcd
pipe_version: HCP_release_20170910_v1.4
oneclick_version: hcponeclick_all
processed: /home/exacloud/lustre1/fnl_lab/data/HCP/processed/
studies:
  - ABCD_GE_SEFMNoT2
subject_pattern: '*NDAR*INV*'
visit_pattern: ses-*
ignored: []
```


# Primary operations

`pipevatt` has several subcommands that hope to grant better understanding of the
status of your pipeline executions for a set of subjects.

## audit

The `audit` subcommand gives a high level overview of the information contained
in each subject's `status.json`. `status.json` is currently our only metric of
success and reflects the status flags listed above. This will return a `csv`.
The `target` column informs you where `audit` believes the pipe crashed.

```
$ pipevatt audit --cfg=./exahead1_abcd.json --blacklist=./blacklist.txt
suid,stid,target,err(KB),out(KB),HcpGenericWrapper,HcpPre,HcpFree,HcpPost,HcpVol,HcpSurf,FNLpreproc_v2,task_fMRI,HcpTask,ExecSummary,ICA_AROMA,HcpSurfICA,FNLpreproc_v2_ICA,task_fMRI_ICA,HcpTask_ICA,ExecSummary_ICA
sub-NDARINVAAAAAAAA,ABCD_PHILIPS_SEFMNoT2,None,None,None,success(1),success(1),success(1),success(1),success(1),success(1),success(1),success(1),unchecked(999),unchecked(999),success(1),success(1),success(1),unchecked(999),success(1),success(1)
sub-NDARINVBBBBBBBB,ABCD_PHILIPS_SEFMNoT2,HcpGenericWrapper,374,201,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None
sub-NDARINVCCCCCCCC,ABCD_PHILIPS_SEFMNoT2,HcpGenericWrapper,2266,1743,failed(3),None,None,None,None,None,None,None,None,None,None,None,None,None,None,None
sub-NDARINVDDDDDDDD,ABCD_PHILIPS_SEFMNoT2,ExecSummary_ICA,1573,950,success(1),success(1),success(1),success(1),success(1),success(1),success(1),success(1),unchecked(999),unchecked(999),success(1),success(1),success(1),unchecked(999),success(1),failed(3)
sub-NDARINVEEEEEEEE,ABCD_PHILIPS_SEFMNoT2,HcpFree,17604,115193,success(1),success(1),failed(3),None,None,None,None,None,None,None,None,None,None,None,None,None
```

This workflow can easily be mixed with tools like `grep` to identify subjects of
common failure points

```
$ pipevatt audit --cfg=./exahead1_abcd.json --blacklist=./blacklist.txt | grep HcpGenericWrapper
suid,stid,target,err(KB),out(KB),HcpGenericWrapper,HcpPre,HcpFree,HcpPost,HcpVol,HcpSurf,FNLpreproc_v2,task_fMRI,HcpTask,ExecSummary,ICA_AROMA,HcpSurfICA,FNLpreproc_v2_ICA,task_fMRI_ICA,HcpTask_ICA,ExecSummary_ICA
sub-NDARINVBBBBBBBB,ABCD_PHILIPS_SEFMNoT2,HcpGenericWrapper,374,201,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None
sub-NDARINVCCCCCCCC,ABCD_PHILIPS_SEFMNoT2,HcpGenericWrapper,2266,1743,failed(3),None,None,None,None,None,None,None,None,None,None,None,None,None,None,None
```

Output can also be written to `.csv` files and opened in `excel` if needed.
```
$ pipevatt audit --cfg=./exahead1_abcd.json --blacklist=./blacklist.txt > output.csv
```


## report

`report` is the most detailed inspection, without just looking directly at the files. We assume that the most critical content will exist in the first and last 15 lines of each log file. In this example we select 3 random subjects to report on.

```bash
$ pipevatt audit --cfg rushmore_adhd.json | awk -F, '{print $1}' | sort | uniq | head -n 3 > current.idt
$ pipevatt report --cfg rushmore_adhd.json --whitelist=current.idt
Reports saved in : /tmp/robinsph.HCPVaTT.RErMzo/
$ tree /tmp/robinsph.HCPVaTT.RErMzo/
/tmp/robinsph.HCPVaTT.RErMzo/
|-- 142345-1
|   `-- 20000000-SIEMENS_TrioTim-Nagel_K_Study
|       |-- FNLpreproc.report
|       |-- HcpFree.report
|       |-- HcpPost.report
|       |-- HcpPre.report
|       |-- HcpSurf.report
|       `-- HcpVol.report
|-- 15150-1
|   |-- 20110512-SIEMENS_TrioTim-Nagel_K_Study
|   |   |-- FNLpreproc.report
|   |   |-- HcpFree.report
|   |   |-- HcpPost.report
|   |   |-- HcpPre.report
|   |   |-- HcpSurf.report
|   |   `-- HcpVol.report
|   |-- 20010611-SIEMENS_TrioTim-Nagel_K_Study
|   |   |-- FNLpreproc.report
|   |   |-- HcpFree.report
|   |   |-- HcpPost.report
|   |   |-- HcpPre.report
|   |   |-- HcpSurf.report
|   |   `-- HcpVol.report
|   `-- 20130706-SIEMENS-Nagel_K-Study
|       |-- FNLpreproc.report
|       |-- HcpFree.report
|       |-- HcpPost.report
|       |-- HcpPre.report
|       |-- HcpSurf.report
|       `-- HcpVol.report
`-- 90750-2
    `-- 20130519-SIEMENS-Nagel_K-Study
        |-- FNLpreproc.report
        |-- HcpFree.report
        |-- HcpPost.report
        |-- HcpPre.report
        |-- HcpSurf.report
        `-- HcpVol.report

8 directories, 30 files
```

# Installation

Any timeyou wish to upgrade to the latest version you can re-run this command without risk

```
$ pip install --user --ignore-installed git+https://gitlab.com/Fair_lab/HCPVaTT.git
$ pipevatt -h
usage: main.py [-h] {audit,sidpath} ...
main.py: error: too few arguments
```


# Library Usecase

```bash
$ ls # we are currently in an empty directory

$ pipevatt config # what configuation files currently exist
exahead1_parkinsons.json
exahead1_theresa.json
RDS_abcd.json
rushmore_adhd.json
exahead1_abcd.json

$ pipevatt config --name=exahead1_abcd.json # retrieve a configuration file
$ ls # verify that the configuration file is present
exahead1_abcd.json
```

For the program `example.py` below.

```python
from pipevatt.sidpath import interface as sidpath

for path in sidpath('./exahead1_abcd.json', 'sub-NDARINVQQQQQQQQ'):
    print(path)
```

```bash
$ python example.py
/home/exacloud/lustre1/fnl_lab/data/HCP/processed/ABCD_GE_SEFMNoT2/sub-NDARINVQQQQQQQQ
```

# Diagnostic notes

If you see many `incomplete(2)` status flags or no `unchecked(999)` status
flags, this is an indicator that the
nodes need their status flags updated, in order to better respect
`expected_outputs`. This can be done with  `hcponeclick_all` by passing a `-U`
flag to it found [here](https://gitlab.com/Fair_lab/hcponeclick). These flags
are most useful if each node's `expected_outputs` has been kept up to date.

If your output case many `None` values in the feed, then you likely have an 
incorrectly formatted configuration file. Always check your configuration file, 
even if you retrieve it from the existing set with the `config` command; as 
these can fall out of date.