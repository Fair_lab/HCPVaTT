#!/usr/bin/env bash

set -x
set -euo pipefail

BASE=/home/users/robinsph/git/HCPVaTT
JQ=/home/users/robinsph/.local/bin/jq
PYTHON=`which python`
CFG=${BASE}/exahead1_abcd.json

if (( $# != 1)); then
    AUDITFILE=`date +%Y-%m-%d`.OUTPUT
    ${PYTHON} ${BASE}/hcpvatt audit --cfg=${CFG} --no-good | tee ${AUDITFILE}
else
    AUDITFILE=$1
fi

DESC=$(cat ${CFG} | ${JQ} .pipeline_descriptor | sed -e 's/^"//' -e 's/"$//')
LABEL=$(cat ${CFG} | ${JQ} .pipeline_label | sed -e 's/^"//' -e 's/"$//')

for node in $(cat ${DESC} | ${JQ} .${LABEL}[])
do
    node=`sed -e 's/^"//' -e 's/"$//' <<< ${node}`
    csv=${node}.csv
    grep -E "(${node},|${node}\$)" ${AUDITFILE} > ${csv}

    dirname=${csv}-dir
    mkdir -p ${dirname}
    ${PYTHON} ${BASE}/hcpvatt report \
               --cfg=${CFG} \
               --whitelist=<(cat ${csv} | awk -F, '{print $1}') \
               --dstdir=${dirname};
done

#for d in *-dir;
#do
#    ${BASE}/organize.sh ${d};
#    mv ${d%%-*} ${d}/
#done

for i in *-dir/*/;
do
    echo "("`ls $i | wc -l`")"  ${i};
done
