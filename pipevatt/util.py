#!/usr/bin/env python

from __future__ import print_function

from collections import namedtuple, OrderedDict
import errno
import functools
import fnmatch
from glob import iglob as glob
from itertools import product, tee
import yaml
import json
from operator import add, attrgetter, itemgetter
import os
import os.path as osp
import re
import sys

status_codes = {
    1:'success(1)',
    2:'incomplete(2)', # this is unusable
    3:'failed(3)',
    4:'not_started(4)',
    999:'unchecked(999)',
    None:None
}

cfg_lookup = {
    '.yml':yaml,
    '.yaml':yaml,
    '.json':json
    }

class UnknownConfigExtentionException(Exception):
    pass

# (A, B, C)(x) => A(B(C(x)))
def compose(*funcs):
    return functools.reduce(lambda f, g: lambda x: f(g(x)), funcs, lambda x: x)

class NoSubjectsException(Exception):
    pass

class MalformedJsonException(Exception):
    pass

def is_iterable(it):
    a, b = tee(it)
    try:
        a.next()
    except StopIteration:
        return False
    return b

def EXIST_CHECK(path):
    if osp.exists(path):
        return path
    raise OSError(errno.ENOENT, os.strerror(errno.ENOENT), path)


logger = 'profile.csv'

def profile(func):
    import time
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        retval = func(*args, **kwargs)
        end = time.time()
        with open(logger, 'a') as fd:
            print(func.__name__, end - start, sep=',', file=fd)
        return retval
    return wrapper


@profile
def _safegetter(getter, key):
    lgetter = (lambda item: None) if key is None else getter(key)

    def function(item):
        return None if item is None else lgetter(item)
    return function


safeattrgetter = functools.partial(_safegetter, attrgetter)
safeitemgetter = functools.partial(_safegetter, itemgetter)


@profile
def get_pipes(config):
    pcfg = get_config('pipeline_descriptor', config)
    label = get_config('pipeline_label', config)
    ignored = get_config('ignored', config)

    retval = get_config(label, pcfg)

    return [x for x in retval if x not in ignored]


@profile
def get_config(key, config, default=None):
    _, ext = os.path.splitext(config)
    try:
        cfglib = cfg_lookup[ext]
    except KeyError:
        raise UnknownConfigExtentionException(ext)

    if not osp.exists(config):
        return default

    with open(config, 'r') as fd:
        return cfglib.load(fd)[key]


@profile
def get_study_paths(config, study=None):
    base = get_config('processed', config)

    if study is None:
        for s in get_config('studies', config):
            yield osp.join(base, s)
    else:
        yield osp.join(base, study)


@profile
def get_scan_paths(config, study=None,
                      blacklist=set(), whitelist=set()):
    targets = whitelist.difference(blacklist)

    if not whitelist:
        paths = _get_all_scan_paths(config, study, blacklist)
    else:
        paths = ids_to_scan_paths(config, *targets)

    paths = is_iterable(paths)
    if not paths:
        raise NoSubjectsException

    for p in paths:
        yield p


def _get_all_scan_paths(config, study=None, blacklist=set()):
    glob_pattern=get_config('subject_pattern', config)
    for spath in get_study_paths(config, study):
        for p in glob(osp.join(spath, glob_pattern)):

            sid = path_to_id(config, p)
            if sid not in blacklist:
                for path in ids_to_scan_paths(config, sid):
                    yield path


def _list_to_pattern_idx(li, re_pattern):
    pattern = re.compile(re_pattern)
    return [idx for idx, _ in enumerate(map(pattern.match, li)) if _]


@profile
def path_to_id(config, path):
    # /path/to/ohsu-sub-NDARINVXXXXXXXX/and/more
    glob_pattern = get_config('subject_pattern', config)
    re_pattern = fnmatch.translate(glob_pattern)
    plist = path.split(osp.sep)

    retval = _list_to_pattern_idx(plist, re_pattern)
    # if there is more than one id in path, that is bad
    return plist[retval[0]]


@profile
def ids_to_scan_paths(config, *sids):
    visit_glob = get_config('visit_pattern', config)

    for sid, spath in product(sids, get_study_paths(config)):
        base = osp.join(spath, sid)
        if not osp.exists(base):
            continue

        for p in glob(osp.join(base, visit_glob)):
            yield p

@profile
def path_to_study(config, path):
    re_sub = fnmatch.translate(get_config('subject_pattern', config))
    plist = path.split(osp.sep)
    retval = _list_to_pattern_idx(plist, re_sub)
    return plist[retval[0]-1] # very fragile


@profile
def path_to_visit(config, path):
    re_sub = fnmatch.translate(get_config('visit_pattern', config))
    plist = path.split(osp.sep)
    retval = _list_to_pattern_idx(plist, re_sub)
    return plist[retval[0]] # very fragile


@profile
def check_status(path):
    # status.cfg exists denotes that a job has at least started.
    config = osp.join(path, 'status.json')
    try:
        retval = get_config('node_status', config)
    except ValueError as e:
        raise MalformedJsonException(config)
    return retval


LogMeta = namedtuple(
    'LogMeta',
    ['size', 'path']
)


PipeMeta = namedtuple(
    'PipeMeta',
    ['path', 'exists', 'status']
)


@profile
def scan_path_to_pipe_meta(config, scan_path, accumulate=True):

    pipe_ver = get_config('pipe_version', config)
    pipe_names = get_pipes(config)
    oclk_ver = get_config('oneclick_version', config)

    table = OrderedDict()
    for pname in pipe_names:
        ppath = osp.join(scan_path, pipe_ver, oclk_ver, pname)

        table[pname] = PipeMeta(ppath, osp.isdir(ppath), check_status(ppath))
    return table
