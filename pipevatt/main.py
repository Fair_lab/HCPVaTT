#!/usr/bin/env python

from __future__ import print_function

import sys

import argparse

import audit
import sidpath
import config

import util


def config_parser(subparsers):
    parser = subparsers.add_parser('config')
    config.generate_parser(parser)
    return parser


def audit_parser(subparsers):
    parser = subparsers.add_parser('audit')
    parser = standards(parser)
    audit.generate_parser(parser)
    return parser


def sid_parser(subparsers):
    parser = subparsers.add_parser('sidpath')
    parser = standards(parser)
    sidpath.generate_parser(parser)
    return parser


def generate_parser(parser):
    subparsers = parser.add_subparsers(help="subcommand")

    audit_parser(subparsers)
    sid_parser(subparsers)
    config_parser(subparsers)
    return parser

def standards(parser):
    parser.add_argument('--cfg', type=util.EXIST_CHECK,
                        help='json for dataset',
                        required=True)
    parser.add_argument('--whitelist', type=str,
                        help="file with one id per line",
                        default=None)
    parser.add_argument('--blacklist', type=str,
                        help='file with one id per line',
                        default='/dev/null')
    return parser



def main():
    parser = argparse.ArgumentParser(
        description='Validation Tool for HCP style pipelines',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser = generate_parser(parser)
    arguments = parser.parse_args()

    if hasattr(arguments, 'func'):
        ret = arguments.func(arguments)
    else:
        ret = parser.print_help()
        sys.exit(ret)

if __name__ == "__main__":
    main()
