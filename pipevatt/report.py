#!/usr/bin/env python2.7

from __future__ import print_function

import functools
import getpass
from glob import iglob as glob
from os import makedirs
import os.path as osp
import tempfile

import util

_topic_sep = '======\n{msg}\n======\n'
_file_sep = '### {msg}\n'


def get_stat_err_out(cfg, subject_path):
    pipes = util.scan_path_to_pipe_meta(cfg, subject_path)

    d = dict()
    for name in pipes:
        base = pipes[name].path
        opath = glob(osp.join(base, name + '*.out'))
        epath = glob(osp.join(base, name + '*.err'))
        d[name] = pipes[name].status, opath, epath

    return d


def take_k_lines(fname, k, rev=False):
    if fname is not None:
        lines = []
        with open(fname, 'r') as fd:
            for i, line in enumerate(fd if not rev else reversed(fd.readlines())):
                if i > k:
                    break
                lines.append(line)
        return '\n'.join(lines)
    else:
        return '***MISSING_FILE*** FROM HCPVaTT'


def interface(cfg, whitelist, tmpdir, length=5):
    print('Reports saved in :', tmpdir)
    paths = util.ids_to_scan_paths(cfg, *whitelist)
    for p in paths: # possible override if non-unique sets
        sid = util.path_to_id(cfg, p)
        sum_meta = get_stat_err_out(cfg, p)
        visit = util.path_to_visit(cfg, p)
        visit_report_path = osp.join(tmpdir, sid, visit)

        if not osp.exists(visit_report_path):
            makedirs(visit_report_path)
        for name in sum_meta:
            st, out, err = sum_meta[name]

            if st is None:
                continue

            with open(osp.join(visit_report_path, name+'.report'), 'w') as fd:
                lprint = functools.partial(print, file=fd)

                lprint(p)
                lprint(util.status_codes[st])
                lprint(_topic_sep.format(msg='stdout'))
                for o in out:
                    lprint(_file_sep.format(msg=o))
                    lprint(take_k_lines(o, length))
                    lprint('...')
                    lprint(take_k_lines(o, length, rev=True))

                lprint(_topic_sep.format(msg='stderr'))
                for e in err:
                    lprint(_file_sep.format(msg=e))
                    lprint(take_k_lines(e, length))
                    lprint('...')
                    lprint(take_k_lines(e, length, rev=True))


def cli_interface(arguments):
    cfg = arguments.cfg
    whitelist = arguments.whitelist
    blacklist = arguments.blacklist

    getlist = lambda fd: set(map(str.strip, fd.readlines()))
    if whitelist is not None:
        with open(whitelist) as wd:
            wl = getlist(wd)
    else:
        wl = set()

    with open(blacklist) as fd:
        bl = getlist(fd)

    tmpdir = arguments.dstdir

    return interface(cfg, wl.difference(bl), tmpdir)


def generate_parser(parser):
    parser.add_argument('--dstdir', type=str,
                        help="file with one id per line",
                        default=tempfile.mkdtemp(prefix=getpass.getuser()+'.HCPVaTT.'))
    parser.add_argument('--number-status', action='store_true',
                        help='yields status codes instead of messages',
                        default=False)


    parser.set_defaults(func=cli_interface)
    return parser
