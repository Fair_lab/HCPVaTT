#!/usr/bin/env python

from __future__ import print_function

import util
from shutil import copyfile
from os import listdir
from os.path import dirname, join

data = join(dirname(__file__), 'configs')


def get_config_list():
    for name in listdir(data):
        yield name


def get_config(name):
    copyfile(join(data, name), join('.', name))


def cli_interface(arguments):
    name = arguments.name
    if name is None:
        for name in get_config_list():
            print(name)
    else:
        get_config(name)


def generate_parser(parser):
    parser.set_defaults(func=cli_interface)
    parser.add_argument('--name', help='name of configuration file', required=False)
    return parser
